/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoaeropuerto;

/**
 *
 * @author Aaron Freire
 */
public class Asiento {
    String numeroAsiento;
    boolean reservado;

    public Asiento(String numeroAsiento) {
        this.numeroAsiento = numeroAsiento;
        reservado=false;
    }

    public String getNumeroAsiento() {
        return numeroAsiento;
    }

    public void setNumeroAsiento(String numeroAsiento) {
        this.numeroAsiento = numeroAsiento;
    }

    public boolean estaReservado() {
        return reservado;
    }

    public void setReservado(boolean reservado) {
        this.reservado = reservado;
    }
    
    
}
