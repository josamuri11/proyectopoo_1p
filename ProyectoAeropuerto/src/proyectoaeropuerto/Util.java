/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoaeropuerto;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jose murillo
 */
public class Util {
    
    public static boolean validadorDeCedula(String cedula) {
        boolean cedulaCorrecta = false;

        try {

            if (cedula.length() == 10) // ConstantesApp.LongitudCedula
            {
                int tercerDigito = Integer.parseInt(cedula.substring(2, 3));
                if (tercerDigito < 6) {
                // Coeficientes de validación cédula
                // El decimo digito se lo considera dígito verificador
                    int[] coefValCedula = { 2, 1, 2, 1, 2, 1, 2, 1, 2 };
                    int verificador = Integer.parseInt(cedula.substring(9,10));
                    int suma = 0;
                    int digito = 0;
                    for (int i = 0; i < (cedula.length() - 1); i++) {
                        digito = Integer.parseInt(cedula.substring(i, i + 1))* coefValCedula[i];
                        suma += ((digito % 10) + (digito / 10));
                    }

                    if ((suma % 10 == 0) && (suma % 10 == verificador)) {
                        cedulaCorrecta = true;
                    }
                    else if ((10 - (suma % 10)) == verificador) {
                     cedulaCorrecta = true;
                    } 
                    else {
                     cedulaCorrecta = false;
                    }
                } 
                else {
                    cedulaCorrecta = false;
                }
            }
            else {
                cedulaCorrecta = false;
            }
        } catch (NumberFormatException nfe) {
            cedulaCorrecta = false;
        } catch (Exception err) {
            System.out.println("Una excepcion ocurrio en el proceso de validadcion");
            cedulaCorrecta = false;
        }

        if (!cedulaCorrecta) {
            System.out.println("La Cédula ingresada es Incorrecta");
        }
        return cedulaCorrecta;
    }
    
    public static String codificarClave(String clave){
        String claveCodificada= Base64.getEncoder().encodeToString(clave.getBytes());
        return claveCodificada;
    }

    public static boolean isNumeric(String strNum) {
        return strNum.matches("-?\\d+(\\.\\d+)?");
    }
    
    public static Date verificarFecha(String fecha){
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String[] splitFecha= fecha.split("-");
        /*if(false && splitFecha.length == 3){
            if(!Util.isNumeric(splitFecha[0])){
                return null;
            }
            if(Integer.parseInt(splitFecha[0]) < 1 || Integer.parseInt(splitFecha[0]) > 30){
                return null;
            }
            
            if(!Util.isNumeric(splitFecha[1])){
                return null;
            }
            if(Integer.parseInt(splitFecha[1]) < 1 || Integer.parseInt(splitFecha[0]) > 12){
                return null;
            }
            
            if(!Util.isNumeric(splitFecha[2])){
                return null;
            }
            if(Integer.parseInt(splitFecha[2]) < 1000){
                return null;
            }                
        }*/
        Date fechaNueva= null;
        try {
            fechaNueva= format.parse(fecha);
        } catch (ParseException ex) {
        }
        if(fechaNueva != null && fechaNueva.compareTo(new Date()) >=0){
            return fechaNueva;
        }
        return null;
    }
    
    public static boolean verificarIATA(String iata){
        if(iata.length() != 3){
            return false;
        }
        for(char c:iata.toCharArray()){
            if(!Character.isUpperCase(c)){
                return false;
            }
        }
        return true;
    }
}
