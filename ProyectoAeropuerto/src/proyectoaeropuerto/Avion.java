/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoaeropuerto;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Aaron Freire
 */
public class Avion {
    private String serie;
    private String fabricante;
    private String modelo;
    private int asientos_negocios;
    private int asientos_economica;
    private int distancia_vuelo;
    private ArrayList<Vuelo> vuelos;

    public Avion(String serie, String fabricante, String modelo, int asientos_negocios, int asientos_economica, int distancia_vuelo) {
        this.serie = serie;
        this.fabricante = fabricante;
        this.modelo = modelo;
        this.asientos_negocios = asientos_negocios;
        this.asientos_economica = asientos_economica;
        this.distancia_vuelo = distancia_vuelo;
        this.vuelos= new ArrayList<>();
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getFabricante() {
        return fabricante;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public int getAsientos_negocios() {
        return asientos_negocios;
    }

    public void setAsientos_negocios(int asientos_negocios) {
        this.asientos_negocios = asientos_negocios;
    }

    public int getAsientos_economica() {
        return asientos_economica;
    }

    public void setAsientos_economica(int asientos_economica) {
        this.asientos_economica = asientos_economica;
    }

    public int getDistancia_vuelo() {
        return distancia_vuelo;
    }

    public void setDistancia_vuelo(int distancia_vuelo) {
        this.distancia_vuelo = distancia_vuelo;
    }

    public ArrayList<Vuelo> getVuelos() {
        return vuelos;
    }    
    
    /**
     * Metodo que carga los datos de aviones a las areolineas correspondientes
     * 
     * @param archivo nombre de archivo donde se sacan los datos
     * @param aerolineas una lista con las aerolineas donde se van a guardar los aviones
     */
    public static void cargarDatosAviones(String archivo,ArrayList<Aerolinea> aerolineas){
        BufferedReader lector;
        try {
            lector = new BufferedReader(
                    new InputStreamReader(new FileInputStream(new File(archivo))));
            String linea;
            while ((linea = lector.readLine())!=null){
                String[] datos = linea.split(",");
                if(!datos.equals("\n")){
                    String serie= datos[0];
                    String fabricante= datos[1];
                    String modelo= datos[2];
                    int asientos_negocios= Integer.parseInt(datos[3]);
                    int asientos_economica= Integer.parseInt(datos[4]);
                    int distancia_maxima= Integer.parseInt(datos[5]);
                    int id_aerolinea= Integer.parseInt(datos[6]);
                    Avion a= new Avion(serie,fabricante,modelo,asientos_negocios,asientos_economica,distancia_maxima);
                    for(Aerolinea aero:aerolineas){
                        if(aero.getCodigo() == id_aerolinea){
                            aero.getAviones().add(a);
                        }
                    }
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Aerolinea.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Aerolinea.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String toString() {
        return "Avion{" + "serie=" + serie + ", fabricante=" + fabricante + ", modelo=" + modelo + ", asientos_negocios=" + asientos_negocios + ", asientos_economica=" + asientos_economica + ", distancia_vuelo=" + distancia_vuelo + '}';
    }
    
    
}
