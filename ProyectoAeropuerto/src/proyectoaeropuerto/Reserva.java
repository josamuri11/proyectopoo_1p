/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoaeropuerto;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author pc
 */
public class Reserva {
    private String codigo;
    private Date fecha;
    private Vuelo vuelo;
    private int codEmpleado;
    private double total;
    private double descuento;
    private ArrayList<Boleto> boletos;

    public Reserva(String codigo, Date fecha, Vuelo vuelo, int codEmpleado, double total, double descuento) {
        this.codigo = codigo;
        this.fecha = fecha;
        this.vuelo = vuelo;
        this.codEmpleado = codEmpleado;
        this.total = total + (total * 0.12);
        this.descuento= descuento;
        this.boletos= new ArrayList<>();
        
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Vuelo getCodVuelo() {
        return vuelo;
    }

    public void setCodVuelo(Vuelo codVuelo) {
        this.vuelo = codVuelo;
    }

    public int getCodEmpleado() {
        return codEmpleado;
    }

    public void setCodEmpleado(int codEmpleado) {
        this.codEmpleado = codEmpleado;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getDescuento() {
        return descuento;
    }

    public void setDescuento(double descuento) {
        this.descuento = descuento;
    }

    public ArrayList<Boleto> getBoletos() {
        return boletos;
    }

    @Override
    public String toString() {
        return "Reserva{" + "codigo=" + codigo + ", fecha=" + fecha + ", codVuelo=" + vuelo + ", codEmpleado=" + codEmpleado + ", total=" + total + ", descuento=" + descuento + ", boletos=" + boletos + '}';
    }
    
    
    
}
