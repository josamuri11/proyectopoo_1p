/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoaeropuerto;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jose murillo
 */
public class Aerolinea {
    private int codigo;
    private String nombre;
    private ArrayList<Avion> aviones;

    public Aerolinea(int codigo,String nombre) {
        this.codigo= codigo;
        this.nombre = nombre;
        aviones= new ArrayList<Avion>();
        
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ArrayList<Avion> getAviones() {
        return aviones;
    }
    
    
    
    public static ArrayList<Aerolinea> cargarAerolineas(String archivo){
        ArrayList<Aerolinea> aerolineas= new ArrayList<>();
        BufferedReader lector;
        try {
            lector = new BufferedReader(
                    new InputStreamReader(new FileInputStream(new File(archivo))));
            String linea;
            while ((linea = lector.readLine())!=null){
                String[] datos = linea.split(",");
                if(!datos.equals("\n")){
                    Aerolinea a=new Aerolinea(Integer.parseInt(datos[0]),datos[1]);
                    aerolineas.add(a);
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Aerolinea.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Aerolinea.class.getName()).log(Level.SEVERE, null, ex);
        }
        return aerolineas;
    }
    
    public static void guardarAerolinea(Aerolinea aero,String archivo){
        try {
            BufferedWriter writer= new BufferedWriter(new FileWriter(new File(archivo),true));
            String datos= aero.getCodigo() + "," + aero.getNombre();
            writer.newLine();
            writer.append(datos);
            writer.close();
            
        } catch (IOException ex) {
        }
    }
    
    
}
