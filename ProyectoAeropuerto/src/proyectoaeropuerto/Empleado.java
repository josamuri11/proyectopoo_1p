/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoaeropuerto;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 *
 * @author jose murillo
 */
public abstract class Empleado {
    protected int codigo_emp;
    protected String cedula;
    protected String nombre;
    protected String apellido;
    protected String email;
    protected Aerolinea aerolinea;
    protected Departamento departamento;
    protected Usuario usuario;

    public Empleado(int codigo_emp, String cedula, String nombre, String apellido, String email, Aerolinea aerolinea, Departamento departamento, Usuario usuario) {
        this.codigo_emp = codigo_emp;
        this.cedula = cedula;
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
        this.aerolinea = aerolinea;
        this.departamento = departamento;
        this.usuario = usuario;
    }

    public Empleado() {
    }

    public int getCodigo_emp() {
        return codigo_emp;
    }

    public void setCodigo_emp(int codigo_emp) {
        this.codigo_emp = codigo_emp;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Aerolinea getAerolinea() {
        return aerolinea;
    }

    public void setAerolinea(Aerolinea aerolinea) {
        this.aerolinea = aerolinea;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public String toString() {
        return "Empleado{" + "codigo_emp=" + codigo_emp + ", cedula=" + cedula + ", nombre=" + nombre + ", apellido=" + apellido + ", email=" + email + ", aerolinea=" + aerolinea + ", departamento=" + departamento + ", usuario=" + usuario + '}';
    }
    
    public static ArrayList<Empleado> cargarEmpleados(String archivoUsuarios,String archivoEmpleados,ArrayList<Aerolinea> aerolineas){
        ArrayList<Empleado> empleados= new ArrayList<>();
        try {   
                ArrayList<Integer> codigos=new ArrayList<>();
                ArrayList<String> cedulas=new ArrayList<>();
                BufferedReader lectorEmpleado  = new BufferedReader(
                new InputStreamReader(new FileInputStream(new File(archivoEmpleados))));
                String lineaEmp;
                while ((lineaEmp = lectorEmpleado.readLine())!=null){
                    String[] datos = lineaEmp.split(",");
                    if (datos.length>1){
                       int codigo= Integer.parseInt(datos[0]);
                       String cedula= datos[1];
                       codigos.add(codigo);
                       cedulas.add(cedula);
                    }
                }
              
                BufferedReader lector  = new BufferedReader(
                new InputStreamReader(new FileInputStream(new File(archivoUsuarios))));
                String linea;
                    while ((linea = lector.readLine())!=null){
                        System.out.println(linea+"---");
                        String[] datos = linea.split(",");
                        if (datos.length>1){
                            String cedula= datos[0];
                            String nombre= datos[1];
                            String apellido= datos[2];
                            String email= datos[3];
                            String usuario= datos[4];
                            String clave= datos[5];
                            String rol= datos[6];
                            String aerolinea= datos[7];
                            String departamento= datos[8];
                            Usuario user= new Usuario(usuario,clave);
                            
                            Aerolinea aero= null;
                            if(!aerolinea.equals("ninguna")){
                                for(Aerolinea a:aerolineas){
                                    if(a.getNombre().equals(aerolinea)){
                                        aero= a;
                                    }
                                }
                            }
                            
                            Departamento dep= null;
                            switch(departamento){
                                case "sistemas":
                                    dep= Departamento.Sistemas;
                                    break;
                                case "comercial":
                                    dep= Departamento.Comercial;
                                    break;
                            }
                            
                            int codigo=-1;
                            for(int i=0;i<codigos.size();i++){
                                if(cedulas.get(i).equals(cedula)){
                                    codigo= codigos.get(i);
                                }
                            }
                            
                            switch(rol){
                                case "A":
                                    Administrador ad=new Administrador(codigo,cedula,nombre,apellido,email,aero,dep,user);
                                    empleados.add(ad);
                                    break;
                                case "P":
                                    Planificador pl=new Planificador(codigo,cedula,nombre,apellido,email,aero,dep,user);
                                    empleados.add(pl);
                                    break;
                                case "C":
                                    Cajero cj=new Cajero(codigo,cedula,nombre,apellido,email,aero,dep,user);
                                    empleados.add(cj);
                                    break;
                            }
                        }
                    }
                
            } catch (FileNotFoundException ex) {
                
            } catch (IOException ex) {
                
            }
        return empleados;

    }
    
    
}
