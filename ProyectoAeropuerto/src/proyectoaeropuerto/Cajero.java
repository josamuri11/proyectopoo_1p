/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoaeropuerto;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author jose murillo
 */
public class Cajero extends Empleado{

    public Cajero(int codigo_emp, String cedula, String nombre, String apellido, String email, Aerolinea aerolinea, Departamento departamento, Usuario usuario) {
        super(codigo_emp, cedula, nombre, apellido, email, aerolinea, departamento, usuario);
    }
    
    public static void mostrarMenuCajero(){
        System.out.println("=====MENU DEL SISTEMA ADMINISTRADOR====");
        System.out.println("1.Venta de boletos");
        System.out.println("2.Salir");
        //System.out.println("3.Salir");
    }
    
    public Destino validarDestino(String opcion,ArrayList<Destino> destinos){
       if(Util.isNumeric(opcion)){
           int op= Integer.parseInt(opcion);
           if(op>0 && op <= destinos.size()){
               return destinos.get(op -1);
           }
       
       }
       System.out.println("!! ERROR, ingrese una de las opciones (numero) que se muestran !!");  
       return null;
    }
    
    public Vuelo validarVuelo(String opcion,ArrayList<Vuelo> vuelos){
       if(Util.isNumeric(opcion)){
           int op= Integer.parseInt(opcion);
           if(op>0 && op <= vuelos.size()){
               return vuelos.get(op -1);
           }
       
       }
       System.out.println("!! ERROR, ingrese una de las opciones (numero) que se muestran !!");
       return null;
    }
    
    
    public Reserva crearReserva(String codigo,Vuelo vuelo,int codEmpleado, double total, double descuento,ArrayList<Boleto> boletos){
        Date fechaReserva= new Date();
        Reserva r= new Reserva(codigo,fechaReserva,vuelo,codEmpleado,total,descuento);
        for(Boleto b:boletos){
            r.getBoletos().add(b);
        }
        this.guardarVenta("ventas.txt",r);
        return r;
    }

    private void guardarVenta(String archivo, Reserva reserva) {
         try {
            BufferedWriter writer= new BufferedWriter(new FileWriter(new File(archivo),true));
            String datos= String.format("%s,%s,%d,%s,%s,%.2f",
                    reserva.getCodigo(),
                    reserva.getCodVuelo().getCodigo(),
                    reserva.getBoletos().size(),
                    reserva.getCodVuelo().getIataSalida(),
                    reserva.getCodVuelo().getIataArribo(),
                    reserva.getTotal() - reserva.getDescuento()
            );
            writer.append(datos);
            writer.newLine();
            writer.close();
            
        } catch (IOException ex) {
        }
    }
}
