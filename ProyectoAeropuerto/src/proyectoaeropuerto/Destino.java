/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoaeropuerto;

import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author pc
 */
public class Destino {
    private String ciudadSalida;
    private String ciudadArribo;
    private ArrayList<Vuelo> vuelos;

    public Destino(String ciudadSalida, String ciudadArribo) {
        this.ciudadSalida = ciudadSalida;
        this.ciudadArribo = ciudadArribo;
        this.vuelos = new ArrayList<>();
    }

    public String getCiudadSalida() {
        return ciudadSalida;
    }

    public void setCiudadSalida(String ciudadSalida) {
        this.ciudadSalida = ciudadSalida;
    }

    public String getCiudadArribo() {
        return ciudadArribo;
    }

    public void setCiudadArribo(String ciudadArribo) {
        this.ciudadArribo = ciudadArribo;
    }

    public ArrayList<Vuelo> getVuelos() {
        return vuelos;
    }
    
    

    @Override
    public String toString() {
        return ciudadSalida + " - " + ciudadArribo;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.ciudadSalida);
        hash = 59 * hash + Objects.hashCode(this.ciudadArribo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Destino other = (Destino) obj;
        if (!Objects.equals(this.ciudadSalida, other.ciudadSalida)) {
            return false;
        }
        if (!Objects.equals(this.ciudadArribo, other.ciudadArribo)) {
            return false;
        }
        return true;
    }
    
    
    
}
