/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoaeropuerto;

import java.util.Base64;
import java.util.Objects;

/**
 *
 * @author jose murillo
 */
public class Usuario {
    String usuario;
    String clave;

    public Usuario(String usuario, String clave) {
        this.usuario = usuario;
        this.clave = clave;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }
    
    public boolean comprobarClave(String clave){
       String claveCodificada= Base64.getEncoder().encodeToString(clave.getBytes());
       if(claveCodificada.equals(this.clave)){
           return true;
       }
       else{
           return false;
       }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.usuario);
        return hash;
    }

    @Override
    public boolean equals(Object user) {
        if (this == user) {
            return true;
        }
        if (user == null) {
            return false;
        }
        if (getClass() != user.getClass()) {
            return false;
        }
        final Usuario otro = (Usuario) user;
        if (this.usuario.equals(otro.usuario) && this.comprobarClave(otro.clave)) {
            return false;
        }
        
            return true;
    }

    @Override
    public String toString() {
        return "Usuario{" + "usuario=" + usuario + ", clave=" + clave + '}';
    }
    
    
}
