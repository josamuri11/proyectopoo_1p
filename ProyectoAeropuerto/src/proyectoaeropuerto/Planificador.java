/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoaeropuerto;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author jose murillo
 */
public class Planificador extends Empleado{

    public Planificador(int codigo_emp, String cedula, String nombre, String apellido, String email, Aerolinea aerolinea, Departamento departamento, Usuario usuario) {
        super(codigo_emp, cedula, nombre, apellido, email, aerolinea, departamento, usuario);
    }
    
    
    public static void mostrarMenuPlanificador(){
        System.out.println("\n=====MENU DEL SISTEMA PLANIFICADOR====\n");
        System.out.println("1.Planificar Vuelos");
        System.out.println("2.Registrar aviones");
        System.out.println("3.Salir");
    }
    
    
    
    
    public boolean registrarAvion(Avion avion,ArrayList<Aerolinea> aerolineas, String archivo){
        for(Aerolinea aero: aerolineas){
            for(Avion a:aero.getAviones()){
                if(a.getSerie().equals(avion.getSerie())){
                    return false;
                }
            }
        }
        
        this.getAerolinea().getAviones().add(avion);
        try {
            BufferedWriter writer= new BufferedWriter(new FileWriter(new File(archivo),true));
            //La forma de escribir los datos es:
            //serie,fabricante,modelo,asientos_negocios,asientos_economica,distancia_maxima,codigo_aerolinea
            String datos= String.format("%s,%s,%s,%d,%d,%d,%d",
                    avion.getSerie(),
                    avion.getFabricante(),
                    avion.getModelo(),
                    avion.getAsientos_negocios(),
                    avion.getAsientos_economica(),
                    avion.getDistancia_vuelo(),
                    this.getAerolinea().getCodigo());
            writer.append(datos);
            writer.newLine();
            writer.close();
            
        } catch (IOException ex) {
        }
        return true;
    }
    
    public Vuelo ingresarVuelo(String codigo, Date fechaSalida, Date fechaArribo, String ciudadSalida, String iataSalida, String ciudadArribo, String iataArribo,Avion avion,ArrayList<Aerolinea> aerolineas){
        for(Avion a:this.aerolinea.getAviones()){
            for(Vuelo v:a.getVuelos()){
                if(v.getCodigo().equals(codigo)){
                    System.out.println("!! No se puede crear el vuelo, ya existe el codigo !!");
                    return null;
                }
            }
        }
        
        int puertaSalida=-1;
        int puertaArribo=-1;
        
        ArrayList<Vuelo> vuelosSalidaSimilar= new ArrayList<>();
        for(Aerolinea aero:aerolineas){
            for(Avion av:aero.getAviones()){
                for(Vuelo v:av.getVuelos()){
                    if(v.getIataSalida().equals(iataSalida)){
                        if(v.getFechaSalida().compareTo(fechaSalida)== 0){
                            vuelosSalidaSimilar.add(v);
                        }
                        else{
                            long diffInMillies = Math.abs(v.getFechaSalida().getTime() - fechaSalida.getTime());
                            long diff = TimeUnit.MINUTES.convert(diffInMillies, TimeUnit.MILLISECONDS);
                            if(diff < 30){
                                vuelosSalidaSimilar.add(v);
                            }
                        }
                    }
                }
            }
        }
        
        ArrayList<Vuelo> vuelosArriboSimilar= new ArrayList<>();
        for(Aerolinea aero:aerolineas){
            for(Avion av:aero.getAviones()){
                for(Vuelo v:av.getVuelos()){
                    if(v.getIataArribo().equals(iataArribo)){
                        if(v.getFechaArribo().compareTo(fechaArribo)== 0){
                            vuelosArriboSimilar.add(v);
                        }
                        else{
                            long diffInMillies = Math.abs(v.getFechaArribo().getTime() - fechaArribo.getTime());
                            long diff = TimeUnit.MINUTES.convert(diffInMillies, TimeUnit.MILLISECONDS);
                            if(diff < 30){
                                vuelosArriboSimilar.add(v);
                            }
                        }
                    }
                }
            }
        }
        
        if(vuelosSalidaSimilar.size() < 8 && vuelosArriboSimilar.size() < 8){
            boolean encontradaSalida=false;
            boolean encontradaArribo=false;
            for(int i=1;i<=8;i++){
                boolean ocupadaSalida=false;
                for(Vuelo v:vuelosSalidaSimilar){
                    if(v.getPuertaSalida() == i){
                        ocupadaSalida=true;
                    }
                }
                if(!ocupadaSalida && !encontradaSalida){
                    puertaSalida= i;
                }
                
                boolean ocupadaArribo=false;
                for(Vuelo v: vuelosArriboSimilar){
                    if(v.getPuertaArribo() == i){
                        ocupadaArribo=true;
                    }
                }
                if(!ocupadaArribo && !encontradaArribo){
                    puertaArribo= i;
                }
            }
        }
        else{
            System.out.println("!! No hay puertas disponibles para este vuelo en la fecha y hora indicada !!");
            return null;
        }
        
        //Como la hora de embarque es 30 minutos antes de la de salida, se crea una nueva fecha
        //a partir de la fecha de salida restandole los millisegundos equivalentes a 30 minutos
        Date fechaEmbarque= new Date(fechaSalida.getTime() - 1800 * 1000);
        
        Vuelo v= new Vuelo(codigo,fechaEmbarque,fechaSalida,fechaArribo,ciudadSalida,iataSalida,ciudadArribo,iataArribo,puertaSalida,puertaArribo,avion);
        this.guardarVuelo(v,"vuelos.txt");
        return v;
    }
    
    private void guardarVuelo(Vuelo v,String archivo){
        DateFormat formater = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        try {
            BufferedWriter writer= new BufferedWriter(new FileWriter(new File(archivo),true));
            //La forma de escribir los datos es:
            //codigo,fechaEmbarque,fechaSalida,fechaArribo,ciudadSalida,iataSalida,ciudadArribo,iataArribo,puertaSalida,puertaArribo,serie_avion
            String datos= String.format("%s,%s,%s,%s,%s,%s,%s,%s,%d,%d,%s",
                    v.getCodigo(),
                    formater.format(v.getFechaEmbarque()),
                    formater.format(v.getFechaSalida()),
                    formater.format(v.getFechaArribo()),
                    v.getCiudadSalida(),
                    v.getIataSalida(),
                    v.getCiudadArribo(),
                    v.getIataArribo(),
                    v.getPuertaSalida(),
                    v.getPuertaArribo(),
                    v.getAvion().getSerie());
            writer.append(datos);
            writer.newLine();
            writer.close();
            
        } catch (IOException ex) {
        }
    }
}
