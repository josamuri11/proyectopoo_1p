/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoaeropuerto;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Aaron Freire
 */
public class Vuelo {
    private String codigo;
    private Date fechaEmbarque;
    private Date fechaSalida;
    private Date fechaArribo;
    private String ciudadSalida;
    private String iataSalida;
    private String ciudadArribo;
    private String iataArribo;
    private int puertaSalida;
    private int puertaArribo;
    private Avion avion;
    private ArrayList<Asiento> asientos_negocios;
    private ArrayList<Asiento> asientos_economica;

    public Vuelo(String codigo, Date fechaEmbarque, Date fechaSalida, Date fechaArribo, String ciudadSalida, String iataSalida, String ciudadArribo, String iataArribo, int puertaSalida, int puertaArribo, Avion avion) {
        this.codigo = codigo;
        this.fechaEmbarque = fechaEmbarque;
        this.fechaSalida = fechaSalida;
        this.fechaArribo = fechaArribo;
        this.ciudadSalida = ciudadSalida;
        this.iataSalida = iataSalida;
        this.ciudadArribo = ciudadArribo;
        this.iataArribo = iataArribo;
        this.puertaSalida = puertaSalida;
        this.puertaArribo = puertaArribo;
        this.avion = avion;
        this.asientos_negocios= new ArrayList<>();
        this.asientos_economica= new ArrayList<>();
        
        for(int i=0;i<avion.getAsientos_negocios();i++){
            String numeroAsiento= "" + (i+1) + "A";
            Asiento a= new Asiento(numeroAsiento);
            this.asientos_negocios.add(a);
        }
        
        for(int i=0;i<avion.getAsientos_economica();i++){
            String numeroAsiento= "" + (i+1) + "B";
            Asiento a= new Asiento(numeroAsiento);
            this.asientos_economica.add(a);
        }
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Date getFechaEmbarque() {
        return fechaEmbarque;
    }

    public void setFechaEmbarque(Date fechaEmbarque) {
        this.fechaEmbarque = fechaEmbarque;
    }

    public Date getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(Date fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public Date getFechaArribo() {
        return fechaArribo;
    }

    public void setFechaArribo(Date fechaArribo) {
        this.fechaArribo = fechaArribo;
    }

    public String getCiudadSalida() {
        return ciudadSalida;
    }

    public void setCiudadSalida(String ciudadSalida) {
        this.ciudadSalida = ciudadSalida;
    }

    public String getIataSalida() {
        return iataSalida;
    }

    public void setIataSalida(String iataSalida) {
        this.iataSalida = iataSalida;
    }

    public String getCiudadArribo() {
        return ciudadArribo;
    }

    public void setCiudadArribo(String ciudadArribo) {
        this.ciudadArribo = ciudadArribo;
    }

    public String getIataArribo() {
        return iataArribo;
    }

    public void setIataArribo(String iataArribo) {
        this.iataArribo = iataArribo;
    }

    public int getPuertaSalida() {
        return puertaSalida;
    }

    public void setPuertaSalida(int puertaSalida) {
        this.puertaSalida = puertaSalida;
    }

    public int getPuertaArribo() {
        return puertaArribo;
    }

    public void setPuertaArribo(int puertaArribo) {
        this.puertaArribo = puertaArribo;
    }

    public Avion getAvion() {
        return avion;
    }

    public void setAvion(Avion avion) {
        this.avion = avion;
    }

    public ArrayList<Asiento> getAsientos_negocios() {
        return asientos_negocios;
    }

    public void setAsientos_negocios(ArrayList<Asiento> asientos_negocios) {
        this.asientos_negocios = asientos_negocios;
    }

    public ArrayList<Asiento> getAsientos_economica() {
        return asientos_economica;
    }

    public void setAsientos_economica(ArrayList<Asiento> asientos_economica) {
        this.asientos_economica = asientos_economica;
    }
    
    public int getAsientosNegociosDisponibles(){
        int asientosDisponibles= 0;
        for(Asiento a:this.getAsientos_negocios()){
            if(a.reservado == false){
                asientosDisponibles++;
            }
        }
        return asientosDisponibles;
    }
    
    public int getAsientosEconomicaDisponibles(){
        int asientosDisponibles= 0;
        for(Asiento a:this.getAsientos_economica()){
            if(a.reservado == false){
                asientosDisponibles++;
            }
        }
        return asientosDisponibles;
    }
    
    
    
    /**
     * Metodo para cargar los vuelos guardados a los aviones de las respectivas aerolineas
     * @param archivo
     * @param aerolineas 
     */
    public static void cargarDatosVuelos(String archivo, ArrayList<Aerolinea> aerolineas){
        BufferedReader lector;
        DateFormat formater = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        try {
            lector = new BufferedReader(
                    new InputStreamReader(new FileInputStream(new File(archivo))));
            String linea;
            while ((linea = lector.readLine())!=null){
                String[] datos = linea.split(",");
                if(!datos.equals("\n")){
                    Date fsalida=null;
                    Date farribo=null;
                    Date fembarque=null;
                    String codigo= datos[0];
                    String fechaEmbarque= datos[1];
                    String fechaSalida= datos[2];
                    String fechaArribo= datos[3];
                    String ciudadSalida= datos[4];
                    String iataSalida= datos[5];
                    String ciudadArribo= datos[6];
                    String iataArribo= datos[7];
                    int puerta_salida= Integer.parseInt(datos[8]);
                    int puerta_arribo= Integer.parseInt(datos[9]);
                    String codigo_avion= datos[10];
                    for(Aerolinea aero:aerolineas){
                        for(Avion a: aero.getAviones()){
                            if(a.getSerie().equals(codigo_avion)){
                                System.out.println("AVION ENCONTRADO!!");
                                fsalida= formater.parse(fechaSalida);
                                farribo= formater.parse(fechaArribo);
                                fembarque= formater.parse(fechaEmbarque);
                                Vuelo v=new Vuelo(codigo,fembarque,fsalida,farribo,ciudadSalida,iataSalida,ciudadArribo,iataArribo,puerta_salida,puerta_arribo,a);
                                a.getVuelos().add(v);
                            }
                        }
                    }
                }
            }
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        } catch (ParseException ex) {
        }
    
    }
}
