/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoaeropuerto;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jose murillo
 */
public class Administrador extends Empleado{

    public Administrador(int codigo_emp, String cedula, String nombre, String apellido, String email, Aerolinea aerolinea, Departamento departamento, Usuario usuario) {
        super(codigo_emp, cedula, nombre, apellido, email, aerolinea, departamento, usuario);
    }
    
    public static void mostrarMenuAdmin(){
        System.out.println("=====MENU DEL SISTEMA ADMINISTRADOR====");
        System.out.println("1.Crear Usuarios");
        System.out.println("2.Crear aerolineas");
        System.out.println("3.Salir");
    }
    
    public boolean validarIngresoCedula(String cedula){
        if(Util.validadorDeCedula(cedula)){
            return true;
        }
        else{
            System.out.println("!! CEDULA INVALIDA, INGRESE DE NUEVO!!\n");
            return false;
        }
    }
    
    public boolean validarIngresoNombre(String nombre){
        for(char c:nombre.toCharArray()){
            if(!Character.isLetter(c)){
                System.out.println("!! NOMBRE INGRESADO INVALIDO, INGRESE DE NUEVO!!\n");
                return false;
            }
        }
        return true;
    }
    
    public boolean validarIngresoClave(String clave){
        boolean tieneMayuscula= false;
        boolean tieneNumero= false;
        boolean tieneLongitudMinima=false;
        for(char c:clave.toCharArray()){
            if(Character.isUpperCase(c)){
                tieneMayuscula= true;
            }
            else if(Character.isDigit(c)){
                tieneNumero= true;
            }
        }
        tieneLongitudMinima= clave.length() >= 8;
        if(tieneMayuscula && tieneNumero && tieneLongitudMinima)
            return true;
        else{
            System.out.println("!! CLAVE INGRESADA INVALIDA, INGRESE DE NUEVO!!\n");
            return false;
        }
    }
    
    
    public String validarIngresoRol(String opcionRol){
        if(opcionRol.equals("1"))
                return "A";
        else if(opcionRol.equals("2"))
                return "P";
        else if(opcionRol.equals("3"))
                return "C";
        else{
                System.out.println("!! Opcion ingresada incorrecta, ingrese el numero de la opcion que desea !!\n");
                return null;
        }
    }
    
    public Aerolinea validarAerolineaAsignada(String opAerolinea,ArrayList<Aerolinea> aerolineas){
        if(Util.isNumeric(opAerolinea)){
            int opcion= Integer.parseInt(opAerolinea);
            if(opcion > 0 && opcion <= aerolineas.size() ){
                return aerolineas.get(opcion -1);
            }
        }
        System.out.println("!! Opcion ingresada incorrecta, ingrese el numero de la opcion que desea !!\n");
        return null;
    }
    
    public Empleado crearUsuario(String cedula,String nombre, String apellido, String clave, String rol,Aerolinea aeroAsignada, Departamento depAsignado,ArrayList<Empleado> empleados){
        for(Empleado e:empleados){
            if(cedula.equals(e.getCedula())){
                return null;
            }
        }
        //Se asumirá que el username es las 3 iniciales del nombre con las 3 del apellido
        String usuario= nombre.substring(0, 3).toLowerCase() + apellido.substring(0,3).toLowerCase();
        System.out.println("\nEl username generado es: "+ usuario);
        
        //El correo será el nombre de usuario junto con la parte del correo de la empresa
        String email= usuario + "@aero.com";
        System.out.println("El email generado es: "+ email +"\n");
        
        //se encripta la clave para no guardar como texto plano
        clave= Util.codificarClave(clave);
        
        //El codigo se incrementa del ultimo usuario que se ingresó
        int codigoUsuario= empleados.get(empleados.size() -1 ).getCodigo_emp() + 1;
        
        //Se crea el usuario para acceder al login
        Usuario user=new Usuario(usuario,clave);
        
        //Se instancia el Tipo de empleado dependiendo del rol que se ingresó
        Empleado emp=null;
        if(rol.equals("A")){
            emp= new Administrador(codigoUsuario,cedula,nombre,apellido,email,aeroAsignada,depAsignado,user);
        }
        else if(rol.equals("P")){
            emp= new Planificador(codigoUsuario,cedula,nombre,apellido,email,aeroAsignada,depAsignado,user);
        }
        else if(rol.equals("C")){
            emp= new Cajero(codigoUsuario,cedula,nombre,apellido,email,aeroAsignada,depAsignado,user);
        }
        
        this.guardarUsuario(emp,rol, "usuarios.txt", "empleados.txt");
        return emp;
    }
    
    private void guardarUsuario(Empleado emp,String rol,String archivoUsuario,String archivoEmpleado){
        
        try {
            //Creamos un escritor pasando como parametro un FileWriter, el cual permite hacer append pasando el
            //segundo parametro del constructor como true
            //Escribimos primero los datos sobre usuarios.txt
            BufferedWriter writer= new BufferedWriter(new FileWriter(new File(archivoUsuario),true));
            //Escribimos los datos en el formato que solicita el proyecto
            String datos= String.format("\n%s,%s,%s,%s,%s,%s,%s,%s,%s",
                    emp.cedula,
                    emp.nombre,
                    emp.apellido,
                    emp.email,
                    emp.getUsuario().getUsuario(),
                    emp.getUsuario().getClave(),
                    rol,
                    emp.getAerolinea() == null ? "ninguna" :emp.getAerolinea().getNombre(), //si aerolinea es null, se escribe ninguna, caso contrario se pasa la aerolinea guardada
                    emp.getDepartamento().toString().toLowerCase());
            writer.append(datos);
            writer.close();
            
            //Escribimos los datos ahora en empleados.txt
            BufferedWriter writer2= new BufferedWriter(new FileWriter(new File(archivoEmpleado),true));
            String datos2= emp.getCodigo_emp() + "," + emp.getCedula();
            writer2.newLine();
            writer2.append(datos2);
            writer2.close();

        } catch (IOException ex) {
            Logger.getLogger(Administrador.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
