/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoaeropuerto;

import java.util.Date;

/**
 *
 * @author pc
 */
public class Boleto {
    private String nombre_cliente;
    private String apellido_cliente;
    private Date fnac_cliente;
    private Asiento asiento;
    private Reserva reserva;

    public Boleto(String nombre_cliente, String apellido_cliente, Date fnac_cliente, Asiento asiento, Reserva reserva) {
        this.nombre_cliente = nombre_cliente;
        this.apellido_cliente = apellido_cliente;
        this.fnac_cliente = fnac_cliente;
        this.asiento = asiento;
        this.reserva = reserva;
    }

    public Boleto() {
    }
    
    

    public String getNombre_cliente() {
        return nombre_cliente;
    }

    public void setNombre_cliente(String nombre_cliente) {
        this.nombre_cliente = nombre_cliente;
    }

    public String getApellido_cliente() {
        return apellido_cliente;
    }

    public void setApellido_cliente(String apellido_cliente) {
        this.apellido_cliente = apellido_cliente;
    }

    public Date getFnac_cliente() {
        return fnac_cliente;
    }

    public void setFnac_cliente(Date fnac_cliente) {
        this.fnac_cliente = fnac_cliente;
    }

    public Asiento getAsiento() {
        return asiento;
    }

    public void setAsiento(Asiento asiento) {
        this.asiento = asiento;
    }

    public Reserva getReserva() {
        return reserva;
    }

    public void setReserva(Reserva reserva) {
        this.reserva = reserva;
    }
    
    
}
