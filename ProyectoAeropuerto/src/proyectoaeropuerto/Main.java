/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoaeropuerto;

import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.Scanner;

/**
 *
 * @author jose murillo
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        ArrayList<Aerolinea> aerolineas= Aerolinea.cargarAerolineas("aerolineas.txt");
        ArrayList<Empleado> empleados= Empleado.cargarEmpleados("usuarios.txt","empleados.txt",aerolineas);
        Avion.cargarDatosAviones("aviones.txt", aerolineas);
        Vuelo.cargarDatosVuelos("vuelos.txt", aerolineas);
        
        for(Empleado e: empleados){
            System.out.println(e);
        }
        
        
        Scanner sc=new Scanner(System.in);
        Empleado emp= null;
        
        do{
            System.out.println(">>>>>>>> SISTEMA DEL AEROPUERTO DE GUAYAQUIL <<<<<<<<<");
            System.out.print("Ingrese su usuario: ");
            String usuario= sc.next();
            System.out.print("Ingrese su clave: ");
            String clave=sc.next();
            for(Empleado e: empleados){
                if(e.getUsuario().getUsuario().equals(usuario) && e.getUsuario().comprobarClave(clave)){
                    emp= e;
                    break;
                }
            }
            if(emp == null){
                System.out.println("!!USUARIO O CLAVE INCORRECTA! INTENTE DE NUEVO!!\n");
            }
            
        }while(emp == null);
        
        sc.nextLine();
        
        if(emp instanceof Administrador){
           Administrador admin= (Administrador) emp;
           String opcion="";
           while(!opcion.equals("3")){
               Administrador.mostrarMenuAdmin();
               
               System.out.print(">> Ingrese una opcion: ");
               opcion= sc.nextLine();
               
               if(opcion.equals("1")){
                   System.out.println("\n@@ CREACION DE USUARIO @@\n");
                   String cedula=null;
                   String nombre=null;
                   String apellido=null;
                   String usuario=null;
                   String clave= null;
                   String email=null;
                   Departamento dep;
                   String rol=null;
                   Aerolinea aero=null;
                   
                   do{
                    System.out.print("> Ingrese la cedula del usuario: ");
                    cedula= sc.next();
                   }while(!admin.validarIngresoCedula(cedula));
                   
                   do{
                    System.out.print("> Ingrese el nombre del usuario: ");
                    nombre= sc.next();
                   }while(!admin.validarIngresoNombre(nombre));
                   
                   do{
                    System.out.print("> Ingrese el apellido del usuario: ");
                    apellido= sc.next();
                   }while(!admin.validarIngresoNombre(apellido));
                   
                   //El usuario y email seran generados automaticamente
                   
                   do{
                    System.out.print("> Ingrese la clave de ingreso del usuario: ");
                    clave= sc.next();
                   }while(!admin.validarIngresoClave(clave));
                                      
                   do{
                    System.out.println("Seleccione el rol del usuario: ");
                    System.out.println("1. Administrador");
                    System.out.println("2. Planificador");
                    System.out.println("3. Cajero");
                    System.out.print("> ");
                    String opcionRol= sc.next();
                    rol= admin.validarIngresoRol(opcionRol);
                   }while(rol== null);
                   
                   if(rol.equals("A")){
                       dep= Departamento.Sistemas;
                   }
                   else{
                       dep= Departamento.Comercial;
                       do{
                        for(int i=0; i<aerolineas.size();i++){
                            System.out.println(String.format("%d. %s", i+1,aerolineas.get(i).getNombre())); 
                        }
                        System.out.print("Escoja una aerolinea para asignar: ");
                        String opAerolinea= sc.next();
                        aero= admin.validarAerolineaAsignada(opAerolinea, aerolineas);
                       }while(aero == null);
                   }
                   
                   Empleado nuevoEmpleado= admin.crearUsuario(cedula, nombre, apellido, clave, rol, aero, dep,empleados);
                   if(nuevoEmpleado!= null){
                    empleados.add(nuevoEmpleado);
                    System.out.println(String.format("El empleado con codigo %d ha sido creado exitosamente!", nuevoEmpleado.getCodigo_emp()));  
                   }
                   else{
                    System.out.println("!!No se puede crear este usuario con la cédula ingresada, ya existe un usuario con ella!!\n");
                   }
                }
                
                else if(opcion.equals("2")){
                    System.out.println("\n@@ CREACION DE AEROLINEAS @@\n");
                    String nombreAerolinea="";
                    do{
                        System.out.print("Ingrese el nombre de la aerolinea: ");
                        nombreAerolinea= sc.nextLine();
                        if(nombreAerolinea.equals("")){
                            System.out.println("!! El nombre ingresado no es correcto, ingrese nuevamente !!");
                        }
                    }while(nombreAerolinea.equals(""));
                    
                    int codigoNuevo= aerolineas.get(aerolineas.size() -1 ).getCodigo() + 1;
                    Aerolinea aero=new Aerolinea(codigoNuevo,nombreAerolinea);
                    aerolineas.add(aero);
                    Aerolinea.guardarAerolinea(aero, "aerolineas.txt");
                    System.out.println(String.format("La aerolinea con codigo %d ha sido creado exitosamente!",codigoNuevo));
                }
           }
        }
        else if(emp instanceof Planificador){
           Planificador planificador= (Planificador) emp;
           String opcion="";
           while(!opcion.equals("3")){
               Planificador.mostrarMenuPlanificador();
               
               System.out.print(">> Ingrese una opcion: ");
               opcion= sc.nextLine();
               
               if(opcion.equals("1")){
                   System.out.println("\n@@ REGISTRO DE VUELOS @@\n");
                   String codigo = null;
                   Date fechaSalida = null;
                   Date fechaArribo = null;
                   String ciudadSalida = null;
                   String iataSalida = null;
                   String ciudadArribo = null;
                   String iataArribo = null;
                   Avion avionSeleccionado= null;
                   
                   do{
                       for(int i=0;i < planificador.getAerolinea().getAviones().size() ;i++){
                           System.out.println(String.format("%d. %s",i+1,planificador.getAerolinea().getAviones().get(i).getModelo()));
                       }
                       System.out.print("\nIngrese el numero del avion a escoger:  ");
                       String opcionAvion= sc.next();
                       if(Util.isNumeric(opcionAvion) && Integer.parseInt(opcionAvion)>0 && Integer.parseInt(opcionAvion) <= planificador.getAerolinea().getAviones().size() ){
                          avionSeleccionado= planificador.getAerolinea().getAviones().get(Integer.parseInt(opcionAvion) - 1);
                       }
                       else{
                           System.out.println("!! La opcion ingresada es incorrecta, ingrese un numero de las opciones mostradas");
                       }
                       
                   }while(avionSeleccionado == null);
                   
                   System.out.print("Ingrese el codigo de vuelo: ");
                   codigo= sc.next();
                   
                   //Aqui se asume que el avion solo puede tener un vuelo por día
                   //por lo que no se puede planificar dos vuelos en un mismo día para un MISMO AVION
                   do{
                    System.out.print("Ingrese la fecha de salida (dd-mm-aaaa): ");
                    String fSalida= sc.next();
                    System.out.print("Ingrese la hora de salida (hh:mm:ss): ");
                    String hSalida=sc.next();
                    Date fecha=Util.verificarFecha(fSalida+ " " + hSalida);
                    if(fecha != null){
                        for(Vuelo v:avionSeleccionado.getVuelos()){
                            if(v.getFechaSalida().getYear() == fecha.getYear() &&
                               v.getFechaSalida().getMonth() == fecha.getMonth() &&
                               v.getFechaSalida().getDay() == fecha.getDay()){
                                //Si la fecha que ingresamos coincide con algun vuelo previo del avion escogido, la desechamos
                                System.out.println("!! No puede ingresar esta fecha, ya está registrada en un vuelo !!");
                                fecha= null;
                                break;
                            }
                        }
                        fechaSalida= fecha;
                    }
                    else{
                        System.out.println("!! Ingrese una fecha y hora valida !!");
                    }
                   }while(fechaSalida == null);
                   
                   do{
                       System.out.print("Ingrese la fecha de Arribo (dd-mm-aaaa): ");
                       String fArribo= sc.next();
                       System.out.print("Ingrese la hora de arribo (hh:mm:ss): ");
                       String hArribo=sc.next();
                       Date fecha=Util.verificarFecha(fArribo+ " " + hArribo);
                       if(fecha != null){
                           if(fecha.compareTo(fechaSalida) > 0){
                               fechaArribo= fecha;
                           }
                           else{
                               System.out.println("!! La fecha y hora de arribo deben ser mayor a la de salida !!");
                           }
                       }
                       else{
                        System.out.println("!! Ingrese una fecha y hora valida !!");
                       }
                   }while(fechaArribo == null);
                   
                   System.out.print("Ingrese la ciudad de salida: ");
                   ciudadSalida= sc.next();
                   System.out.print("Ingrese la ciudad de arribo: ");
                   ciudadArribo= sc.next();
                   
                   do{
                       System.out.println("Ingrese el codigo IATA de 3 digitos de salida: ");
                       iataSalida= sc.next();
                       if(!Util.verificarIATA(iataSalida)){
                           System.out.println("!! Ingreso incorrecto, el codigo IATA solo puede tener 3 Letras mayusculas !!");
                       }
                   }while(Util.verificarIATA(iataSalida) != true);
                   
                   do{
                       System.out.println("Ingrese el codigo IATA de 3 digitos de arribo: ");
                       iataArribo= sc.next();
                       if(Util.verificarIATA(iataArribo) && !iataArribo.equals(iataSalida)){
                           break;
                       }
                       else{
                           System.out.println("!! Ingreso incorrecto, el codigo IATA solo puede tener 3 Letras mayusculas y no puede ser igual al de salida !!");
                       }
                   }while(true);
                   
                   Vuelo v=planificador.ingresarVuelo(codigo, fechaSalida, fechaArribo, ciudadSalida, iataSalida, ciudadArribo, iataArribo,avionSeleccionado, aerolineas);
                   if(v != null){
                       avionSeleccionado.getVuelos().add(v);
                       System.out.println(String.format("Vuelo con puerta de salida %d y puerta de arribo %d creado exitosamente!!",v.getPuertaSalida(),v.getPuertaArribo()));
                   }
               }
               
               else if(opcion.equals("2")){
                   System.out.println("\n@@ REGISTRO DE AVIONES @@\n");
                   String serie=null;
                   String fabricante=null;
                   String modelo=null;
                   String asientos_negocios=null;
                   String asientos_economica=null;
                   String distancia_vuelo=null;
                   
                   System.out.print("Ingrese el numero de serie: ");
                   serie=sc.next();
                   
                   sc.nextLine();
                   do{
                       System.out.print("Ingrese el fabricante: ");
                       fabricante=sc.nextLine();
                       if(fabricante.equals("")){
                           System.out.println("!! Ingreso incorrecto, no puede dejar vacío !!\n");
                       }
                   }while(fabricante.equals(""));
                   
                   do{
                       System.out.print("Ingrese el modelo: ");
                       modelo= sc.nextLine();
                       if(modelo.equals("")){
                           System.out.println("!! Ingreso incorrecto, no puede dejar vacío !!\n");
                       }
                   }while(modelo.equals(""));
                   
                   do{
                       System.out.print("Ingrese la capacidad de asientos de primera clase: ");
                       asientos_negocios= sc.next();
                       if(Util.isNumeric(asientos_negocios) && Integer.parseInt(asientos_negocios)>0){
                           break;
                       }
                       else{
                           System.out.println("!! Ingreso incorrecto, debe ingresar un numero mayor a cero");
                       }
                   }while(true);
                   
                   do{
                       System.out.print("Ingrese la capacidad de asientos de segunda clase: ");
                       asientos_economica= sc.next();
                       if(Util.isNumeric(asientos_economica) && Integer.parseInt(asientos_economica)>0){
                           break;
                       }
                       else{
                           System.out.println("!! Ingreso incorrecto, debe ingresar un numero mayor a cero");
                       }
                   }while(true);
                   
                   do{
                       System.out.print("Ingrese la distancia maxima de vuelo en KM: ");
                       distancia_vuelo= sc.next();
                       if(Util.isNumeric(distancia_vuelo) && Integer.parseInt(distancia_vuelo)>0){
                           break;
                       }
                       else{
                           System.out.println("!! Ingreso incorrecto, debe ingresar un numero mayor a cero");
                       }
                   }while(true);
                   
                   Avion avion= new Avion(serie,fabricante,modelo,Integer.parseInt(asientos_negocios),Integer.parseInt(asientos_economica),Integer.parseInt(distancia_vuelo));
                   boolean avionRegistrado= planificador.registrarAvion(avion,aerolineas, "aviones.txt");
                   if(avionRegistrado){
                       System.out.println("El avion ha sido registrado exitosamente!");
                   }
                   else{
                       System.out.println("!! Ingreso incorrecto, el número de serie ya existe !!");
                   }
               }
           }
        }
        else if(emp instanceof Cajero){
           Cajero cajero= (Cajero) emp;
           String opcion="";
           while(!opcion.equals("2")){
               Cajero.mostrarMenuCajero();
               
               System.out.print(">> Ingrese una opcion: ");
               opcion= sc.next();
               
               if(opcion.equals("1")){
                   System.out.println("\n@@@ VENTA DE BOLETOS @@@");
                   ArrayList<Destino> destinosDisponibles=new ArrayList<>();
                   for(Avion a:cajero.getAerolinea().getAviones()){
                       for(Vuelo v:a.getVuelos()){
                           Destino d= new Destino(v.getCiudadSalida(),v.getCiudadArribo());
                           if(v.getAsientosEconomicaDisponibles()>0 || v.getAsientosNegociosDisponibles() > 0){
                               if(!destinosDisponibles.contains(d)){
                                   d.getVuelos().add(v);
                                   destinosDisponibles.add(d);
                               }
                               else{
                                   int indice= destinosDisponibles.indexOf(d);
                                   destinosDisponibles.get(indice).getVuelos().add(v);
                               }
                           } 
                       }
                   }
                   
                   Destino destinoEscogido=null;
                   do{
                    for(int i=0;i<destinosDisponibles.size();i++){
                           System.out.println(String.format("%d. %s", i+1,destinosDisponibles.get(i)));
                    }
                       System.out.print("Seleccione un destino: ");
                    String opcionDestino= sc.next();
                    destinoEscogido= cajero.validarDestino(opcionDestino, destinosDisponibles);
                   }while(destinoEscogido == null);
                   
                   Vuelo vueloEscogido=null;
                   do{
                    for(int i=0;i<destinoEscogido.getVuelos().size();i++){
                           System.out.println(String.format("%d. %s asientos disp. negocios: %d, asientos disp. economica: %d, fecha y hora salida: %s", 
                                   i+1,
                                   destinoEscogido.getVuelos().get(i),
                                   destinoEscogido.getVuelos().get(i).getAsientosNegociosDisponibles(),
                                   destinoEscogido.getVuelos().get(i).getAsientosEconomicaDisponibles(),
                                   destinoEscogido.getVuelos().get(i).getFechaSalida()
                           ));
                    }
                    System.out.print("Seleccione un vuelo: ");
                    String opcionVuelo= sc.next();
                    vueloEscogido= cajero.validarVuelo(opcionVuelo, destinoEscogido.getVuelos());
                   }while(vueloEscogido == null);
                   
                   System.out.print("Ingrese el codigo de la reserva: ");
                   String codigoReserva= sc.next();
                   
                   int cantBoletosEconomica=-1;
                   int cantBoletosNegocios=-1;
                   double precioBoletosEconomica=-1;
                   double precioBoletosNegocios=-1;
                   
                   
                   if(vueloEscogido.getAsientosNegociosDisponibles() > 0){
                       do{
                        System.out.print("Ingrese la cantidad de boletos a comprar en clase de Negocios: ");
                        String cantBoletos= sc.next();
                        if(Util.isNumeric(cantBoletos) && Integer.parseInt(cantBoletos) <= vueloEscogido.getAsientosNegociosDisponibles()){
                            cantBoletosNegocios= Integer.parseInt(cantBoletos);
                        }
                        else{
                            System.out.println("!! Ingrese un cantidad correcta que no sea mayor a la disponible!!");
                        }
                       }while(cantBoletosNegocios == -1);
                       
                       do{
                           System.out.println("Ingrese el valor del boleto de clase de Negocios: ");
                           String precio= sc.next();
                           if(Util.isNumeric(precio) && Double.parseDouble(precio) > 0){
                            precioBoletosNegocios= Double.parseDouble(precio);
                        }
                        else{
                            System.out.println("!! Ingrese un cantidad correcta mayor a cero!!");
                        }
                       }while(precioBoletosNegocios == -1);
                   }
                   else{
                       System.out.println("!! No hay boletos para la clase de Negocios!");
                       cantBoletosNegocios= 0;
                   }
                   
                   if(vueloEscogido.getAsientosEconomicaDisponibles() > 0){
                       do{
                        System.out.print("Ingrese la cantidad de boletos a comprar en clase de Economcia: ");
                        String cantBoletos= sc.next();
                        if(Util.isNumeric(cantBoletos) && Integer.parseInt(cantBoletos) <= vueloEscogido.getAsientosEconomicaDisponibles()){
                            cantBoletosEconomica= Integer.parseInt(cantBoletos);
                        }
                        else{
                            System.out.println("!! Ingrese un cantidad correcta que no sea mayor a la disponible!!");
                        }
                       }while(cantBoletosEconomica == -1);
                       
                       do{
                           System.out.println("Ingrese el valor del boleto de clase Economica: ");
                           String precio= sc.next();
                           if(Util.isNumeric(precio) && Double.parseDouble(precio) > 0){
                            precioBoletosEconomica= Double.parseDouble(precio);
                        }
                        else{
                            System.out.println("!! Ingrese un cantidad correcta mayor a cero!!");
                        }
                       }while(precioBoletosEconomica== -1);
                   }
                   else{
                       System.out.println("!! No hay boletos para la clase Economica!");
                       cantBoletosEconomica= 0;
                   }
                   
                   ArrayList<Boleto> boletos= new ArrayList<>();
                   
                   System.out.println("INGRESO DE BOLETOS DE CLASE ECONOMICA");
                   for(int i=0;i< cantBoletosEconomica;i++){
                       boletos.add(new Boleto());
                   }
                   
                   System.out.println("INGRESO DE BOLETOS DE CLASE NEGOCIOS");
                   for(int i=0;i< cantBoletosEconomica;i++){
                       boletos.add(new Boleto());                  
                   }
                   
                   double total= (precioBoletosEconomica * cantBoletosEconomica) + (precioBoletosNegocios * cantBoletosNegocios);
                   
                   cajero.crearReserva(codigoReserva, vueloEscogido, cajero.getCodigo_emp(), precioBoletosNegocios,0,boletos);
                   
                   System.out.println("Reserva creada con éxito");
               }
           }
        }
        
    }
    
}
