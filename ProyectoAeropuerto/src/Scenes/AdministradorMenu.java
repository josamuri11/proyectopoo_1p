package Scenes;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import proyectoaeropuerto.Administrador;
import proyectoaeropuerto.Aerolinea;
import proyectoaeropuerto.Departamento;
import proyectoaeropuerto.Empleado;
import proyectoaeropuerto.Usuario;

/**
 *
 * @author joseMurillo
 */
public class AdministradorMenu {
    private static Aerolinea ar=null;
    public static Scene vetanaAdmin(Stage st,Scene scp,Administrador adm){
        Label labeltit=new Label("MENU DEL SISTEMA ADMINISTRADOR");
        StackPane sp1=new StackPane();
        sp1.getChildren().add(labeltit);
        
        Button btnus=new Button("Crear Usuarios");
        StackPane sp2=new StackPane();
        sp2.getChildren().add(btnus);
        
        Button btnar=new Button("Crear Aerolineas");
        StackPane sp3=new StackPane();
        sp3.getChildren().add(btnar);
        
        Button btnsalir=new Button("Salir");
        StackPane sp4=new StackPane();
        sp4.getChildren().add(btnsalir);
        
        VBox vb=new VBox(20);
        vb.getChildren().addAll(sp1,sp2,sp3,sp4);
        
        Scene sc=new Scene(vb,300,300);
        
        btnsalir.setOnMouseClicked((MouseEvent e)->{
            st.setTitle("Log In");
            st.setScene(scp);
        });
        
        btnus.setOnMouseClicked((MouseEvent e)->{
            st.setTitle("Crear Usuario");
            st.setScene(crearUsuario(st,sc,adm));
        });
        
        btnar.setOnMouseClicked((MouseEvent e)->{
            st.setTitle("Crear Aerolinea");
            st.setScene(crearAerolinea(st,sc));
        });
        
        return sc;
    }
    
    public static Scene crearUsuario(Stage st, Scene scp, Administrador adm){
        VBox vb=new VBox(10);
        
        Label lbtit =new Label("Creacion Usuario");
        StackPane sp=new StackPane();
        sp.getChildren().add(lbtit);
        
        Label lbced=new Label("Cedula: ");
        TextField txtced=new TextField();
        
        Label lbnom=new Label("Nombre: ");
        TextField txtnom=new TextField();
        
        Label lbap=new Label("Apellido: ");
        TextField txtap=new TextField();
        
        Label lbcla=new Label("Clave: ");
        PasswordField txtcla=new PasswordField();
        
        GridPane gp=new GridPane();
        gp.setAlignment(Pos.CENTER);
        gp.setHgap(10);
        gp.setVgap(10);
        gp.add(lbced, 0, 0);
        gp.add(txtced, 1, 0);
        gp.add(lbnom, 0, 1);
        gp.add(txtnom, 1, 1);
        gp.add(lbap, 0, 2);
        gp.add(txtap, 1, 2);
        gp.add(lbcla, 0, 3);
        gp.add(txtcla, 1, 3);
        
        ArrayList<String> listrol=new ArrayList<>();
        listrol.add("Administrador");
        listrol.add("Planificador");
        listrol.add("Cajero");
        ComboBox cbrol=new ComboBox(FXCollections.observableList(listrol));
        cbrol.getSelectionModel().selectFirst();
        StackPane sp2=new StackPane();
        sp2.getChildren().add(cbrol);
        
        HBox hb=new HBox(20);
        Button btngua=new Button("Guardar");
        Button btnsalir=new Button("Salir");
        hb.getChildren().addAll(btngua,btnsalir);
        hb.setAlignment(Pos.CENTER);
        
        
        //Eventos
        
        cbrol.setOnAction((e)->{
            String sel=(String)cbrol.getSelectionModel().getSelectedItem();
            if(!sel.equalsIgnoreCase("Administrador")){
                if(vb.getChildren().size()<5){
                    ComboBox cbaer=new ComboBox(FXCollections.observableList(Aerolinea.listAerolinea));
                    cbaer.getSelectionModel().selectFirst();
                    ar=(Aerolinea) cbaer.getSelectionModel().getSelectedItem();
                    StackPane sp3=new StackPane();
                    sp3.getChildren().add(cbaer);
                    vb.getChildren().add(3, sp3);
                     cbaer.setOnAction((e2)->{
                         ar=(Aerolinea)cbaer.getSelectionModel().getSelectedItem();
                     });
                }
            }else{
                vb.getChildren().remove(3);
                ar=null;
            }
        });
        
        btnsalir.setOnMouseClicked((MouseEvent e)->{
            st.setTitle("Administrador Menu");
            st.setScene(scp);
        });
        
        btngua.setOnMouseClicked((MouseEvent e)->{
            GuardarUsuario(txtced.getText(),txtnom.getText(),txtap.getText(),txtcla.getText(),(String)cbrol.getSelectionModel().getSelectedItem(),adm);
            txtced.clear();
            txtnom.clear();
            txtap.clear();
            txtcla.clear();
        });
        
        vb.getChildren().addAll(sp,gp,sp2,hb);
        Scene sc=new Scene(vb,400,400);
        return sc;
    }
    
    private static void GuardarUsuario(String cedula,String nombre,String apellido,String clave,String rol, Administrador adm){
        
        Departamento dep;
        if(rol.equalsIgnoreCase("Administrador")){
            dep=Departamento.Sistemas;
        }else{
            dep=Departamento.Comercial;
        }
        
        if(!adm.validarIngresoCedula(cedula)){
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("ERROR");
            alert.setContentText("No se escribio la cedula de manera correcta");
            alert.showAndWait();
        }else if(!adm.validarIngresoNombre(nombre)){
            //mensaje alerta
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("ERROR");
            alert.setContentText("No se escribio el nombre de manera correcta");
            alert.showAndWait();
        }else if(!adm.validarIngresoClave(clave)){
            //mensaje alerta
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("ERROR");
            alert.setContentText("No se escribio la clave de manera correcta");
            alert.showAndWait();
        }else if(!adm.validarIngresoNombre(apellido)){
            //mensaje alerta
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("ERROR");
            alert.setContentText("No se escribio el apellido de manera correcta");
            alert.showAndWait();
        }else{
            Empleado emp=adm.crearUsuario(cedula, nombre, apellido, clave, rol.substring(0, 1), ar, dep, Empleado.listEmpleado);
            if(emp!=null){
                //guardarUsuario
                Usuario.listUsuario.add(emp.getUsuario());
                Usuario.GuardarUsuarios();
                //guardarempleado
                Administrador.listEmpleado.add(emp);
                Administrador.GuardarEmpleados();
            }else{
                //alerta que ya existe un empleado con esa cedula
                Alert alert = new Alert(AlertType.ERROR);
                alert.setTitle("Error Dialog");
                alert.setHeaderText("ERROR");
                alert.setContentText("Ya existe un empleado con esa cedula");
                alert.showAndWait();
            }
        }
    }
    public static Scene crearAerolinea(Stage st, Scene scp){
        VBox vb=new VBox(10);
        
        Label lbtit=new Label("Creacion de Aerolineas");
        StackPane sp=new StackPane();
        sp.getChildren().add(lbtit);
        
        Label lbnom=new Label("Nombre: ");
        TextField txtnom=new TextField();
        HBox hb=new HBox(10);
        hb.getChildren().addAll(lbnom,txtnom);
        hb.setAlignment(Pos.CENTER);
        
        Button btnguar=new Button("Guardar");
        Button btnsal=new Button("Cancelar");
        HBox hb2=new HBox(10);
        hb2.getChildren().addAll(btnguar,btnsal);
        hb2.setAlignment(Pos.CENTER);
        vb.getChildren().addAll(sp,hb,hb2);
        
        btnsal.setOnMouseClicked((MouseEvent e)->{
            st.setTitle("Administrador Menu");
            st.setScene(scp);
        });
        
        btnguar.setOnMouseClicked((MouseEvent e)->{
            guardarAerolinea(txtnom.getText());
            txtnom.clear();
        });
        
        Scene sc=new Scene(vb,300,300);
        return sc;
        
    }
    
    private static void guardarAerolinea(String nom){
        if(nom.equalsIgnoreCase("")){
            //alerta no se ingreso nombre
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("ERROR");
            alert.setContentText("No se escribio un nombre para Aerolinea");
            alert.showAndWait();
        }else{
            int codigo= Aerolinea.listAerolinea.get(Aerolinea.listAerolinea.size()-1).getCodigo()+1;
            Aerolinea aero=new Aerolinea(codigo,nom);
            Aerolinea.listAerolinea.add(aero);
            Aerolinea.GuardarAeroLinea();
        }
    }