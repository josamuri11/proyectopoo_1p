package Scenes;

import static Scenes.AdministradorMenu.crearAerolinea;
import static Scenes.AdministradorMenu.crearUsuario;
import java.util.Date;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import proyectoaeropuerto.Administrador;
import proyectoaeropuerto.Aerolinea;
import proyectoaeropuerto.Avion;
import proyectoaeropuerto.Planificador;
import proyectoaeropuerto.Util;
import proyectoaeropuerto.Vuelo;

/**
 *
 * @author aronFreire
 */
public class PlanificadorMenu {
    
    public static Scene ventanaPlanificador(Stage st, Scene scp,Planificador plan){
        Label labeltit=new Label("MENU DEL SISTEMA PLANIFICADOR");
        StackPane sp1=new StackPane();
        sp1.getChildren().add(labeltit);
        
        Button btnrv=new Button("Registro Vuelos");
        StackPane sp2=new StackPane();
        sp2.getChildren().add(btnrv);
        
        Button btnra=new Button("Registro Aviones");
        StackPane sp3=new StackPane();
        sp3.getChildren().add(btnra);
        
        Button btnsalir=new Button("Salir");
        StackPane sp4=new StackPane();
        sp4.getChildren().add(btnsalir);
        
        VBox vb=new VBox(20);
        vb.getChildren().addAll(sp1,sp2,sp3,sp4);
        
        Scene sc=new Scene(vb,300,300);
        
        btnsalir.setOnMouseClicked((MouseEvent e)->{
            st.setTitle("Log In");
            st.setScene(scp);
        });
        
        btnrv.setOnMouseClicked((MouseEvent e)->{
            st.setTitle("Registrar Vuelos");
            st.setScene(crearVuelos(st,sc,plan));
        });
        
        btnra.setOnMouseClicked((MouseEvent e)->{
            st.setTitle("Registrar Aviones");
            st.setScene(crearAviones(st,sc,plan));
        });
        
        return sc;
        
        
    }
    
    
    public static Scene crearVuelos(Stage st, Scene scp, Planificador plan){
        VBox vb=new VBox(15);
        Label lbtit=new Label("Registrar Vuelos");
        StackPane sp=new StackPane();
        sp.getChildren().add(lbtit);
        GridPane gp=new GridPane();
        gp.setAlignment(Pos.CENTER);
        gp.setHgap(10);
        gp.setVgap(10);
        
        Label lbav=new Label("Escoja el avion: ");
        ComboBox cb=new ComboBox(FXCollections.observableList(plan.getAerolinea().getAviones()));
        cb.getSelectionModel().selectFirst();
        
        Label lbco=new Label("Ingrese el codigo: ");
        TextField txtco=new TextField();
        
        Label lbfs=new Label("Ingrese fecha de salida(dd-mm-aaaa): ");
        TextField txtfs=new TextField();
        
        Label lbhs=new Label("Ingrese hora de salida(hh:mm:ss)");
        TextField txths=new TextField();
        
        Label lbfa=new Label("Ingrese fecha de arribo(dd-mm-aaaa): ");
        TextField txtfa=new TextField();
        
        Label lbha=new Label("Ingrese hora de arribo(hh:mm:ss):");
        TextField txtha=new TextField();
        
        Label lbcs=new Label("Ingrese la ciudad de salida: ");
        TextField txtcs=new TextField();
        
        Label lbca=new Label("Ingrese la ciudad de arribo:");
        TextField txtca=new TextField();
        
        Label lbcis=new Label("Ingrese codigo IATA de 3 digitos de salia: ");
        TextField txtcis=new TextField();
        
        Label lbcia=new Label("Ingrese codigo IATA de 3 digitos de arribo: ");
        TextField txtcia=new TextField();
        
        Button btngua=new Button("Guardar");
        Button btncan=new Button("Cancelar");
        
        gp.add(lbav, 0, 0);
        gp.add(cb, 1, 0);
        
        gp.add(lbco, 0, 1);
        gp.add(txtco, 1, 1);
        
        gp.add(lbfs, 0, 2);
        gp.add(txtfs, 1, 2);
        
        gp.add(lbhs, 0, 3);
        gp.add(txths, 1, 3);
        
        gp.add(lbfa, 0, 4);
        gp.add(txtfa, 1, 4);
        
        gp.add(lbha, 0, 5);
        gp.add(txtha, 1, 5);
        
        gp.add(lbcs, 0, 6);
        gp.add(txtcs, 1, 6);
        
        gp.add(lbca, 0, 7);
        gp.add(txtca, 1, 7);
        
        gp.add(lbcis, 0, 8);
        gp.add(txtcis, 1, 8);
        
        gp.add(lbcia, 0, 9);
        gp.add(txtcia, 1, 9);
        
        gp.add(btngua, 0, 10);
        gp.add(btncan, 1, 10);
        
        btngua.setOnMouseClicked((MouseEvent e)->{
            guardarVuelo((Avion)cb.getSelectionModel().getSelectedItem(),txtco.getText(),txtfs.getText(),txths.getText(),txtfa.getText(),txtha.getText(),txtcs.getText(),txtca.getText(),txtcis.getText(),txtcia.getText(),plan);
        });
        
        btncan.setOnMouseClicked((MouseEvent e)->{
            st.setTitle("Planificador Menu");
            st.setScene(scp);
        });
        
        vb.getChildren().addAll(sp,gp);
        Scene sc=new Scene(vb,1100,500);
        return sc;
    }
    
    private static void guardarVuelo(Avion av,String codigo, String fs,String hs, String fa, String ha, String cs, String ca,String is, String ia,Planificador plan){
        if(codigo.equalsIgnoreCase("") || fs.equalsIgnoreCase("") || hs.equalsIgnoreCase("") || fa.equalsIgnoreCase("")|| ha.equalsIgnoreCase("") || cs.equalsIgnoreCase("") || ca.equalsIgnoreCase("")|| is.equalsIgnoreCase("") || ia.equalsIgnoreCase("")){
            //alerta de que faltan datos
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("ERROR");
            alert.setContentText("Faltan Datos");
            alert.showAndWait();
        }else{
            Date fecha=Util.verificarFecha(fs+ " " + hs);
            if(fecha!=null){
                boolean banderafava=true;
                for(Vuelo v:av.getVuelos()){
                    if(v.getFechaSalida().getYear() == fecha.getYear() &&
                        v.getFechaSalida().getMonth() == fecha.getMonth() &&
                        v.getFechaSalida().getDay() == fecha.getDay()){
                        //Si la fecha que ingresamos coincide con algun vuelo previo del avion escogido, la desechamos
                        banderafava=false;
                    }
                }
                
                if(banderafava){
                    Date fechaa=Util.verificarFecha(fa+ " " + ha);
                    if(fechaa!= null){
                           if(!(fechaa.compareTo(fecha) > 0)){
                               //alerta fecha de arribo es menor a la fecha de salida
                               Alert alert = new Alert(Alert.AlertType.ERROR);
                               alert.setTitle("Error Dialog");
                               alert.setHeaderText("ERROR");
                               alert.setContentText("Fecha de arribo es memor a la fecha de salida");
                               alert.showAndWait();
                           }
                           else{
                               if(Util.verificarIATA(is)){
                                   if(Util.verificarIATA(ia) && !is.equals(ia)){
                                       //Crear vuelo
                                       Vuelo v=plan.ingresarVuelo(codigo, fecha, fechaa, cs, is, ca, ia,av, Aerolinea.listAerolinea);
                                       if(v!=null){
                                           av.getVuelos().add(v);
                                           Aerolinea.GuardarAeroLinea();
                                           Administrador.GuardarEmpleados();
                                           //alerta de informacion
                                           //System.out.println(String.format("Vuelo con puerta de salida %d y puerta de arribo %d creado exitosamente!!",v.getPuertaSalida(),v.getPuertaArribo()));
                                       }
                                   }else{
                                       //alerta ia no es valido o es igual a is
                                       Alert alert = new Alert(Alert.AlertType.ERROR);
                                       alert.setTitle("Error Dialog");
                                       alert.setHeaderText("ERROR");
                                       alert.setContentText("Codio Iata Arribo no es valido o es igual al Codigo iata de Salida");
                                       alert.showAndWait();
                                   }
                               }else{
                                   //alerta is no es valido
                                   Alert alert = new Alert(Alert.AlertType.ERROR);
                                   alert.setTitle("Error Dialog");
                                   alert.setHeaderText("ERROR");
                                   alert.setContentText("Codigo Iata Salida no es valido");
                                   alert.showAndWait();
                               }
                           }
                    }else{
                        //alerta fecha de arribo es invalida
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Error Dialog");
                        alert.setHeaderText("ERROR");
                        alert.setContentText("Fecha de Arribo no es valida");
                        alert.showAndWait();
                    }
                }else{
                    //alerta fecha coincide con un vuelvo previo
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error Dialog");
                    alert.setHeaderText("ERROR");
                    alert.setContentText("Fecha de Salida coincide con un vuelo previo");
                    alert.showAndWait();
                }
                
            }else{
                //alerta la fecha de salida es invalida
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error Dialog");
                alert.setHeaderText("ERROR");
                alert.setContentText("Fecha de Salida no es valida");
                alert.showAndWait();
            }
        }
            
    public static Scene crearAviones(Stage st, Scene scp, Planificador plan){
        VBox vb=new VBox(15);
        
        Label lbtit=new Label("REGISTRO DE AVIONES");
        StackPane sp=new StackPane();
        sp.getChildren().add(lbtit);
        
        GridPane gp=new GridPane();
        gp.setAlignment(Pos.CENTER);
        gp.setVgap(10);
        gp.setHgap(10);
        
        Label lbser=new Label("Ingrese el numero de serie: ");
        TextField txtser=new TextField();
        
        Label lbfa=new Label("Ingrese Fabricante: ");
        TextField txtfa=new TextField();
        
        Label lbmo=new Label("Ingrese Modelo: ");
        TextField txtmo=new TextField();
        
        Label lbpc=new Label("Ingrese capacidad de asientos primera clase: ");
        TextField txtpc=new TextField();
        
        Label lbsc=new Label("Ingrese capacidad de asientos segunda clase: ");
        TextField txtsc=new TextField();
        
        Label lbkm=new Label("Ingrese la distancia maxima de vuelo en KM: ");
        TextField txtkm=new TextField();
        
        Button btngua=new Button("Guardar");
        Button btncan=new Button("Cancelar");
        
        gp.add(lbser, 0, 0);
        gp.add(txtser, 1, 0);
        
        gp.add(lbfa, 0, 1);
        gp.add(txtfa, 1, 1);
        
        gp.add(lbmo, 0, 2);
        gp.add(txtmo, 1, 2);
        
        gp.add(lbpc, 0, 3);
        gp.add(txtpc, 1, 3);
        
        gp.add(lbsc, 0, 4);
        gp.add(txtsc, 1, 4);
        
        gp.add(lbkm, 0, 5);
        gp.add(txtkm, 1, 5);
        
        gp.add(btngua, 0, 6);
        gp.add(btncan, 1, 6);
        
        btngua.setOnMouseClicked((MouseEvent e)->{
            guardarAvion(txtser.getText(),txtfa.getText(),txtmo.getText(),txtpc.getText(),txtsc.getText(),txtkm.getText(),plan);
        });
        
        btncan.setOnMouseClicked((MouseEvent e)->{
            st.setTitle("Planificador Menu");
            st.setScene(scp);
        });
        
        vb.getChildren().addAll(sp,gp);
        Scene sc=new Scene(vb,500,500);
        return sc;
    }
    
    public static void guardarAvion(String serie,String fabricante, String modelo, String an, String ae,String km,Planificador plan){
        if(serie.equalsIgnoreCase("") || fabricante.equalsIgnoreCase("")||modelo.equalsIgnoreCase("") ||an.equalsIgnoreCase("") ||ae.equalsIgnoreCase("") ||km.equalsIgnoreCase("")){
            //alerta de que faltan datos
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("ERROR");
            alert.setContentText("Faltan datos");
            alert.showAndWait();
        }else{
            try{
                Avion avion=new Avion(serie,fabricante,modelo,Integer.parseInt(an),Integer.parseInt(ae),Integer.parseInt(km));
                boolean registrado=plan.registrarAvion(avion, Aerolinea.listAerolinea, "nada");
                if(registrado){//modificar metodo registrarAvion
                    //alerta de que se hizo
                }else{
                    //alerta de que no
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error Dialog");
                    alert.setHeaderText("ERROR");
                    alert.setContentText("Ya existe el avion");
                    alert.showAndWait();
                }
            }catch(Exception e){
                //alerta de que no se puso un numero de manera correcta
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error Dialog");
                alert.setHeaderText("ERROR");
                alert.setContentText("Se introdujo letras en vez de numeros");
                alert.showAndWait();
            }
        }
    }
    }