package Scenes;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import static java.lang.Thread.sleep;
import java.time.LocalDate;
import java.time.ZoneId;
import static java.time.temporal.TemporalQueries.localDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import proyectoaeropuerto.Avion;
import proyectoaeropuerto.Vuelo;

/**
 *
 * @author Jmurillo
 */
public class PantallaVuelos {
    
    public static void avionesDespegar(){
        /*TableView tvAvion=new TableView();
        
        TableColumn<String, Avion> column1 = new TableColumn<>("Id");
        column1.setCellValueFactory(new PropertyValueFactory<>("id"));
        column1.setPrefWidth(100);
        TableColumn<String, Avion> column2 = new TableColumn<>("Estado");
        column2.setCellValueFactory(new PropertyValueFactory<>("estadoEntrega"));
        column2.setPrefWidth(200);
        TableColumn<String, Avion> column3 = new TableColumn<>("Direccion");
        column3.setCellValueFactory(new PropertyValueFactory<>("direccion"));
        column3.setPrefWidth(200);
        
        tvAvion.getColumns().addAll(column1,column2,column3);*/
        Group g=new Group();
        ImageView im=LogIn.obtenerFondo();
        im.setFitHeight(500);
        im.setFitWidth(703);
        VBox vb=new VBox(20);
        Label lbtit=new Label("PARTIDAS");
        lbtit.setFont(Font.loadFont("file:Gabo.otf",40));
        StackPane sp=new StackPane();
        
        sp.getChildren().addAll(lbtit);
        TableView tvVuelo=new TableView();
        TableColumn<String, Avion> column1 = new TableColumn<>("Codigo");
        column1.setCellValueFactory(new PropertyValueFactory<>("codigo"));
        column1.setPrefWidth(100);
        TableColumn<String, Avion> column3 = new TableColumn<>("Ciudad Arribo");
        column3.setCellValueFactory(new PropertyValueFactory<>("ciudadArribo"));
        column3.setPrefWidth(200);
        TableColumn<String, Avion> column4 = new TableColumn<>("Puerta Salida");
        column4.setCellValueFactory(new PropertyValueFactory<>("puertaSalida"));
        column4.setPrefWidth(200);
        
        TableColumn<String, Avion> column2 = new TableColumn<>("Hora de Salida");
        column2.setCellValueFactory(new PropertyValueFactory<>("fechaSalida"));
        column2.setPrefWidth(200);
        
        tvVuelo.getColumns().addAll(column1,column2,column3,column4);
        
        HBox hb=new HBox();
        hb.getChildren().addAll(/*tvAvion,*/tvVuelo);
        vb.getChildren().addAll(sp,hb);
        vb.setLayoutY(15);
        g.getChildren().addAll(im,vb);
        Scene sc=new Scene(g,700,480);
        Stage st=new Stage();
        st.setScene(sc);
        st.setTitle("Vuelos Por Despegar");
        st.show();
        Runnable rn=()->refrescarTabla(tvVuelo);
        Thread tr=new Thread(rn);
        tr.start();
    }
    
    public static void refrescarTabla(TableView tv){
        while(true){
            actualizarVuelos(tv);
            try {
                sleep(10);
            } catch (InterruptedException ex) {
                Logger.getLogger(PantallaVuelos.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    //Calcula y muestra los vuelos que salgan en los proximos 30 minutos
    public static void actualizarVuelos(TableView tv){
        ArrayList<Vuelo> lv=Vuelo.listVuelos;
        TreeSet<Vuelo> treev=new TreeSet<>(Vuelo.listVuelos);
         ArrayList<Vuelo> lvr=new ArrayList<>();
        for(Vuelo v:treev){
            //obtengo mi date actual
       //   Date date = java.sql.Date.valueOf(LocalDate.now());
            //obtengo el tiempo de una media hora antes de que el vuelo empiece
           // System.out.println(date);
        //    System.out.println(date.getTime());
        //    long limite=v.getFechaEmbarque().getTime()- 1800 * 1000;
       //     System.out.println(limite);
      //      if(limite<=date.getTime() && date.getTime()<=v.getFechaEmbarque().getTime()){// si es que falta media hora para el embarque entonces se agrega a la lista
                lvr.add(v);
     //       }
        }
        Platform.runLater(()->tv.setItems(FXCollections.observableList(lvr)));
        Platform.runLater(()->tv.refresh());
        
    }
}
