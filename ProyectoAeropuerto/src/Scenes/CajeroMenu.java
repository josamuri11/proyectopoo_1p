package Scenes;

import java.util.ArrayList;
import java.util.Date;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import proyectoaeropuerto.Asiento;
import proyectoaeropuerto.AsientoImagen;
import proyectoaeropuerto.Avion;
import proyectoaeropuerto.Boleto;
import proyectoaeropuerto.Cajero;
import proyectoaeropuerto.Destino;
import proyectoaeropuerto.Reserva;
import proyectoaeropuerto.Vuelo;

/**
 *
 * @author AFreire
 */
public class CajeroMenu {
    
    public static Scene vetanaCajero(Stage st,Scene scp,Cajero cj){
        Group g=new Group();
        ImageView im=LogIn.obtenerFondo();
        im.setFitHeight(300);
        im.setFitWidth(300);
        
        Label labeltit=new Label("MENU CAJERO");
        labeltit.setFont(Font.loadFont("file:Gabo.otf",30));
        StackPane sp1=new StackPane();
        sp1.getChildren().add(labeltit);
        
        Button btnven=new Button("Venta de Boletos");
        StackPane sp2=new StackPane();
        sp2.getChildren().add(btnven);
        
        Button btnsalir=new Button("Salir");
        StackPane sp4=new StackPane();
        sp4.getChildren().add(btnsalir);
        
        VBox vb=new VBox(30);
        vb.getChildren().addAll(sp1,sp2,sp4);
        vb.setLayoutX(70);
        vb.setLayoutY(15);
        g.getChildren().addAll(im,vb);
        Scene sc=new Scene(g,300,300);
        
        btnsalir.setOnMouseClicked((MouseEvent e)->{
            st.setTitle("Log In");
            st.setScene(scp);
        });
        
        btnven.setOnMouseClicked((MouseEvent e)->{
            st.setTitle("Venta de Boletos");
            st.setScene(ventaBoletos(cj,st,sc));
        });
        
        
        
        return sc;
    }
    
    public static Scene ventaBoletos(Cajero cajero,Stage st,Scene scp){
        Group g=new Group();
        ImageView im=LogIn.obtenerFondo();
        im.setFitHeight(875);
        im.setFitWidth(875);
        
        VBox vb=new VBox(30);
        Label lbtit=new Label("Reserva de Tickets");
        lbtit.setFont(Font.loadFont("file:Gabo.otf",30));
        StackPane sp1=new StackPane();
        sp1.getChildren().add(lbtit);
        
        GridPane gp=new GridPane();
        gp.setVgap(10);
        gp.setHgap(10);
        
        Label lbd= new Label("Seleccione un destino:");
        ComboBox cbd=new ComboBox(FXCollections.observableList(getDestinos(cajero)));
        cbd.getSelectionModel().selectFirst();
        gp.add(lbd, 0, 0);
        gp.add(cbd, 1, 0);
        
        Label lbv=new Label("Seleccione un vuelo");
        ComboBox cbv= new ComboBox(FXCollections.observableList(getVuelos((Destino) cbd.getSelectionModel().getSelectedItem())));
        cbv.getSelectionModel().selectFirst();
        cbv.getSelectionModel().selectFirst();
        gp.add(lbv, 0, 1);
        gp.add(cbv, 1, 1);
        
        Label lbc=new Label("Ingrese Codigo de Reserva:");
        TextField txtc=new TextField();
        gp.add(lbc, 0, 2);
        gp.add(txtc, 1, 2);
        
        Button btnsal=new Button("Salir");
        Button btncomp=new Button("Comprar Asientos");
        gp.add(btnsal, 0, 3);
        gp.add(btncomp, 1, 3);
        
        vb.getChildren().addAll(sp1,gp);
        vb.setLayoutX(20);
        vb.setLayoutY(15);
        g.getChildren().addAll(im,vb);
        Scene sc=new Scene(g,875,250);
        cbd.setOnAction((e)->{
            cbv.setItems(FXCollections.observableList(getVuelos((Destino) cbd.getSelectionModel().getSelectedItem())));
            cbv.getSelectionModel().selectFirst();
        });
        
        btnsal.setOnMouseClicked((MouseEvent e)->{
            st.setTitle("Menu Cajero");
            st.setScene(scp);
        });
        
        btncomp.setOnMouseClicked((MouseEvent e)->{
            if(!txtc.getText().equalsIgnoreCase("")){
                st.setTitle("Reservar Asientos");
                st.setScene(reservarAsientos(cajero,st,sc, (Vuelo) cbv.getSelectionModel().getSelectedItem(),txtc.getText(),scp));
            }else{
                //alerta no se puso codigo
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error Dialog");
                alert.setHeaderText("ERROR");
                alert.setContentText("No se escribio el codigo para la reserva");
                alert.showAndWait();
            }
        });
        
        return sc;
        
    }
    public static Scene reservarAsientos(Cajero caj,Stage st, Scene scp,Vuelo v,String cod,Scene menu){
        Group g=new Group();
        ImageView im=LogIn.obtenerFondo();
        im.setFitHeight(500);
        im.setFitWidth(450);
        
        ArrayList<AsientoImagen> lai=new ArrayList<>();
        VBox vb=new VBox(30);
        Label lbtit=new Label("Elegir Asientos");
        lbtit.setFont(Font.loadFont("file:Gabo.otf",30));
        StackPane sp=new StackPane();
        sp.getChildren().addAll(lbtit);
        GridPane gpan=new GridPane();
        if(v.getAsientos_negocios().size()%6==0){
            ubicarAsientos(gpan,0,v.getAsientos_negocios(),lai,"Negocio",v);
        }else{
            ubicarAsientos(gpan,v.getAsientos_negocios().size()%6,v.getAsientos_negocios(),lai,"Negocio",v);
        }
        
        GridPane gpae=new GridPane();
        if(v.getAsientos_economica().size()%6==0){
            ubicarAsientos(gpae,0,v.getAsientos_economica(),lai,"Economico",v);
        }else{
            ubicarAsientos(gpae,v.getAsientos_economica().size()%6,v.getAsientos_economica(),lai,"Economico",v);
        }
        
        GridPane gpinfo=new GridPane();
        agregarInfo(gpinfo);
        GridPane gppadre=new GridPane();
        gppadre.setAlignment(Pos.CENTER);
        gppadre.setHgap(40);
        
        Label lbi=new Label("Informacion Asientos");
        lbi.setFont(Font.loadFont("file:Gabo.otf",15));
        Label lbn=new Label("Seccion Negocio");
        lbn.setFont(Font.loadFont("file:Gabo.otf",15));
        Label lbe=new Label("Seccion Economica");
        lbe.setFont(Font.loadFont("file:Gabo.otf",15));
        gppadre.add(lbi,0, 0);
        gppadre.add(lbn, 1, 0);
        gppadre.add(gpinfo, 0, 1);
        gppadre.add(gpan,1 , 1);
        gppadre.add(lbe, 1, 2);
        gppadre.add(gpae, 1, 3);
        
        Button btnsalir=new Button("Regresar");
        Button btncomp=new Button("Finalizar Compra");
        HBox hb=new HBox(30);
        hb.getChildren().addAll(btnsalir,btncomp);
        hb.setAlignment(Pos.CENTER);
        vb.getChildren().addAll(sp,gppadre,hb);
        vb.setLayoutX(20);
        vb.setLayoutY(15);
        g.getChildren().addAll(im,vb);
        Scene sc=new Scene(g,450,500);
        
        btnsalir.setOnMouseClicked((MouseEvent e)->{
            st.setTitle("Venta Boletos");
            st.setScene(scp);
        });
        
        btncomp.setOnMouseClicked((MouseEvent e)->{
            st.setTitle("Finalizar Compra");
            if(!lai.isEmpty()){
                st.setScene(finalizarCompra(st,sc,lai,caj,cod,v,menu));
            }else{
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error Dialog");
                alert.setHeaderText("ERROR");
                alert.setContentText("No se ha seleccionado ningun asiento");
                alert.showAndWait();
            }
        });
        
        return sc;
    }
    
    private static void ubicarAsientos(GridPane gp,int residuo,ArrayList<Asiento> la,ArrayList<AsientoImagen> lai,String tipo,Vuelo v){
        
        for(Reserva r:Reserva.listR){
            if(v.equals(r.getCodVuelo())){
                
                for(Boleto b: r.getBoletos()){
                    Asiento a=b.getAsiento();
                    for(Asiento as:la){
                        if(as.equals(a) && a.estaReservado()){
                            as.setReservado(true);
                        }
                    }
                }
            }
        }        
        
        int fila=0;
        int columna=0;
        for(Asiento a:la){
            if(a.estaReservado()){
                AsientoImagen ai=new AsientoImagen(Asiento.getAsientoReservado(),false,a,tipo);
                ai.setFitHeight(30);
                ai.setFitWidth(30);
                gp.add(ai, columna, fila);
            }else{
                AsientoImagen ai=new AsientoImagen(Asiento.getAsientoDisponible(),false,a,tipo);
                ai.setFitHeight(30);
                ai.setFitWidth(30);
                gp.add(ai, columna, fila);
                ai.setOnMouseClicked((MouseEvent e)->{ 
                   if(ai.isSeleccionado()){
                       ai.setSeleccionado(false);
                       ai.setImage(Asiento.getAsientoDisponible().getImage());
                       lai.remove(ai);
                   }else{
                       ai.setSeleccionado(true);
                       ai.setImage(Asiento.getAsientoSeleccionado().getImage());
                       lai.add(ai);
                   }
                });
            }
            
            if(columna>=5 || (columna>=(residuo-1) && fila==0 && residuo!=0)){
                
                columna=0;
                fila+=1; 
            }else{
                columna+=1;
            }
        }
    }
    
    public static Scene finalizarCompra(Stage st, Scene scp,ArrayList<AsientoImagen> lai,Cajero caj,String codigo, Vuelo vuelo,Scene menu){
        Group g=new Group();
        ImageView im=LogIn.obtenerFondo();
        im.setFitHeight(450);
        im.setFitWidth(500);
        
        VBox vb=new VBox(30);
        
        Label lbtit=new Label("Ingreso de Datos");
        lbtit.setFont(Font.loadFont("file:Gabo.otf",30));
        StackPane sp=new StackPane();
        sp.getChildren().addAll(lbtit);
        
        GridPane gp=new GridPane();
        gp.setHgap(20);
        gp.setVgap(20);
        Label lbnom=new Label("Nombre:");
        TextField txtnom=new TextField();
        gp.add(lbnom, 0, 0);
        gp.add(txtnom, 1, 0);
        Label lbap=new Label("Apellido:");
        TextField txtap=new TextField();
        gp.add(lbap, 0, 1);
        gp.add(txtap, 1, 1);
        Label lbfn=new Label("Fecha de Nacimiento:");
        DatePicker dp=new DatePicker();
        gp.add(lbfn, 0, 2);
        gp.add(dp, 1, 2);
        int cantneg=0;
        int cantec=0;
        for(AsientoImagen ai:lai){
            if(ai.isSeleccionado()){
                if(ai.getTipo().equalsIgnoreCase("Negocio")){
                    cantneg+=1;
                }else{
                    cantec+=1;
                }
            }
        }
        
        Label lban=new Label("Total asientos seccion negocio:");
        TextField txtan=new TextField();
        txtan.setText(String.valueOf(cantneg));
        txtan.setEditable(false);
        gp.add(lban, 0, 3);
        gp.add(txtan, 1, 3);
        Label lbec=new Label("Total asientos seccion economica:");
        TextField txtec=new TextField();
        txtec.setText(String.valueOf(cantec));
        txtec.setEditable(false);
        gp.add(lbec, 0, 4);
        gp.add(txtec, 1, 4);
        
        int pagar= 50*cantneg +25*cantec;
        Label lbp=new Label("Total a pagar:");
        TextField txtp=new TextField();
        txtp.setText(String.valueOf(pagar));
        txtp.setEditable(false);
        gp.add(lbp, 0, 5);
        gp.add(txtp, 1, 5);
        Button btnsalir=new Button("Regresar");
        Button btncomp=new Button("Finalizar Compra");
        gp.add(btnsalir, 0, 6);
        gp.add(btncomp, 1, 6);
        
        btnsalir.setOnMouseClicked((MouseEvent e)->{
            st.setTitle("Reservar Asientos");
            st.setScene(scp);
        });
        
        btncomp.setOnMouseClicked((MouseEvent e)->{
            if(dp.getValue()!=null || !txtnom.getText().equalsIgnoreCase("") || !txtap.getText().equalsIgnoreCase("")){
                ArrayList<Boleto> lb=crearBoletos(lai,txtnom.getText(),txtap.getText(),java.sql.Date.valueOf(dp.getValue()));
                crearReserva(codigo,vuelo,pagar,lb,caj);
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Information Dialog");
                alert.setHeaderText(null);
                alert.setContentText("Se ha reservado los boletos exitosamente!");

                alert.showAndWait();
                st.setTitle("Cajero Menu");
                st.setScene(menu);
            }else{
                //alerta
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error Dialog");
                alert.setHeaderText("ERROR");
                alert.setContentText("No se ha llenado toda informacion necesaria");
                alert.showAndWait();
            }
            
        });
        
        vb.getChildren().addAll(sp,gp);
        gp.setAlignment(Pos.CENTER);
        vb.setLayoutX(65);
        vb.setLayoutY(15);
        g.getChildren().addAll(im,vb);
        Scene sc=new Scene(g,500,450);
        return sc;
    }
    
    private static ArrayList<Boleto> crearBoletos(ArrayList<AsientoImagen> lai, String nombre, String apellido, Date fc){
        ArrayList<Boleto> lb=new ArrayList<>();
        for(AsientoImagen ai:lai){
            if(ai.isSeleccionado()){
                Asiento a=ai.getA();
                a.setReservado(true);
                Boleto b=new Boleto(nombre,apellido,fc,a);
                lb.add(b);
                }
            }
        
        return lb;
    }
    
        private static void crearReserva(String codigo,Vuelo v,int total, ArrayList<Boleto> lb,Cajero cj){
        Reserva r=cj.crearReserva(codigo, v, cj.getCodigo_emp(), total, 0, lb);
        r.setCodVuelo(v);
        for(Boleto b:lb){
            b.setReserva(r);
        }
    }
    
    private static void agregarInfo(GridPane gp){
            Label lbd=new Label("Asiento Disponible");
            Label lbr=new Label("Asiento Reservado");
            Label lbs=new Label("Asiento Seleccionado");
            gp.add(lbd, 1, 0);
            gp.add(Asiento.getAsientoDisponible(), 0, 0);
            gp.add(lbs, 1, 1);
            gp.add(Asiento.getAsientoSeleccionado(), 0, 1);
            gp.add(lbr, 1, 2);
            gp.add(Asiento.getAsientoReservado(), 0, 2);
    
    }
    
    
    private static ArrayList<Destino> getDestinos(Cajero cajero){
        ArrayList<Destino> destinosDisponibles=new ArrayList<>();
                   for(Avion a:cajero.getAerolinea().getAviones()){
                       for(Vuelo v:a.getVuelos()){
                           Destino d= new Destino(v.getCiudadSalida(),v.getCiudadArribo());
                           if(v.getAsientosEconomicaDisponibles()>0 || v.getAsientosNegociosDisponibles() > 0){
                               if(!destinosDisponibles.contains(d)){
                                   d.getVuelos().add(v);
                                   destinosDisponibles.add(d);
                               }
                               else{
                                   int indice= destinosDisponibles.indexOf(d);
                                   destinosDisponibles.get(indice).getVuelos().add(v);
                               }
                           } 
                       }
                   }
                   return destinosDisponibles;
    }
    
    private static ArrayList<Vuelo> getVuelos(Destino d){
        ArrayList<Vuelo> descpV=new ArrayList<>();
        for(int i=0;i<d.getVuelos().size();i++){
                Vuelo v=d.getVuelos().get(i);
                String s= String.format("%d. asientos disp. negocios: %d, asientos disp. economica: %d, fecha y hora salida: %s", 
                i+1,
                
                v.getAsientosNegociosDisponibles(),
                v.getAsientosEconomicaDisponibles(),
                v.getFechaSalida()
                );
                descpV.add(v);
                           
        }
        return descpV;
    }
    
}
    
    